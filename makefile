################################################################################
# Build the core FreeRTOS as a static lib
################################################################################

TRGT = arm-none-eabi-
CC   = $(TRGT)gcc
CP   = $(TRGT)objcopy
BIN  = $(CP) -O ihex
AR   = $(TRGT)ar

MCU = cortex-m4
FPU = -mfloat-abi=softfp -mfpu=fpv4-sp-d16 -D__FPU_USED=1

MCFLAGS = -mthumb -mcpu=$(MCU) $(FPU)

# Define optimisation level here
OPT = -Os

CFLAGS=-DDEBUG -D__CODE_RED -D__USE_LPCOPEN -DCORE_M4 -D__MULTICORE_NONE -D__NEWLIB__ \
  -I"inc" -I"inc/ipv4" -I"../lpc_chip_43xx/inc" -I"../lpc_board_ea_oem_4357/inc" -I"../lpc4357_freertos_lib/inc" \
  -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fsingle-precision-constant -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -mthumb -D__NEWLIB__ -specs=redlib.specs -MMD -MP
LDFLAGS=-lrt



# Add inputs and outputs from these tool invocations to the build variables
SOURCES = \
src/api/api_lib.c \
src/api/api_msg.c \
src/api/err.c \
src/api/netbuf.c \
src/api/netdb.c \
src/api/netifapi.c \
src/api/sockets.c \
src/api/tcpip.c

SOURCES += \
src/arch/lpc18xx_43xx_emac.c \
src/arch/lpc_debug.c \
src/arch/sys_arch_freertos.c

SOURCES += \
src/netif/etharp.c \
src/netif/ethernetif.c


OBJECTS=$(SOURCES:.c=.o)

LWIPLIB=liblwip_lpc4357.a

all: $(SOURCES) $(LWIPLIB)

$(LWIPLIB): $(OBJECTS)
	$(AR) $(ARFLAGS) $(LWIPLIB)  $?

.c.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f src/*.o $(LWIPLIB)


